﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShardingSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var store = DocumentSharding.CreateShardedStore();
            
            //using (var session = store.OpenSession())
            //{
            //    var companies = Initilization.CreateCompanies();

            //    foreach (var item in companies)
            //    {
            //        session.Store(item);
            //    }

            //    var products = Initilization.CreateProducts();

            //    foreach (var item in products)
            //    {
            //        session.Store(item);
            //    }

            //    session.Store(new Invoice { CompanyId = companies.First().Id, Products = products.Take(2).Select(x => x.Id).ToList(), IssuedAt = DateTime.Today.AddDays(-5), Amount = products.Take(2).Sum(x => x.Price) });
            //    session.Store(new Invoice { CompanyId = companies.Skip(1).Take(1).First().Id, Products = products.Select(x => x.Id).ToList(), IssuedAt = DateTime.Today.AddDays(-20), Amount = products.Sum(x => x.Price) });
            //    session.Store(new Invoice { CompanyId = companies.Skip(2).Take(1).First().Id, Products = products.Take(6).Select(x => x.Id).ToList(), IssuedAt = DateTime.Today.AddDays(-8), Amount = products.Take(6).Sum(x => x.Price) });
            //    session.Store(new Invoice { CompanyId = companies.Skip(1).Take(1).First().Id, Products = products.Take(8).Select(x => x.Id).ToList(), IssuedAt = DateTime.Today.AddDays(-1), Amount = products.Take(8).Sum(x => x.Price) });
            //    session.Store(new Invoice { CompanyId = companies.Last().Id, Products = products.Take(5).Select(x => x.Id).ToList(), IssuedAt = DateTime.Today.AddDays(-2), Amount = products.Take(5).Sum(x => x.Price) });
            //    session.SaveChanges();
            //}
            using (var session = store.OpenSession())
            {
                //get all, should automagically retrieve from each shard
                var allCompanies = session.Query<Company>()
                    .Customize(x => x.WaitForNonStaleResultsAsOfNow())
                    .Where(company => company.Region == "Asia")
                    .ToArray();

                foreach (var company in allCompanies)
                    Console.WriteLine(company.Name);
            }
        }
    }
}
