﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShardingSample
{
    class Invoice
    {
       
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public double Amount { get; set; }
        public DateTime IssuedAt { get; set; }
        public List<string> Products { get; set; }
    }
}
