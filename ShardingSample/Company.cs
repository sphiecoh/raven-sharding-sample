﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShardingSample
{
    class Company
    {
        
        public string Id { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
    }
}
