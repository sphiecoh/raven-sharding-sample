﻿using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Shard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShardingSample
{
    public class DocumentSharding
    {
        public static IDocumentStore CreateShardedStore()
        {
            var shads = new Dictionary<string, IDocumentStore>
            {
                {"Asia" , new DocumentStore { Url ="http://localhost:8088" }},
                {"Africa" , new DocumentStore { Url ="http://localhost:8089" }},
                {"Europe" , new DocumentStore { Url ="http://localhost:8087" }},
            };

            var shardStratergy = new ShardStrategy(shads).
                                     ShardingOn<Company>(x => x.Region).
                                     ShardingOn<Product>(x => x.Region)
                                     .ShardingOn<Invoice>(x => x.CompanyId);

            return new ShardedDocumentStore(shardStratergy).Initialize();

        }
    }
}
