﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShardingSample
{
    class Initilization
    {
        public static IList<Product> CreateProducts()
        {
            return (new [] { 
              new Product{Name ="Nikon Camera",Price= 200.15, Region ="Africa" },
               new Product{Name ="Diesle Jean",Price= 1200.00, Region ="Africa" },
                new Product{Name ="Dixon Headphone",Price= 200.15, Region ="Europe" },
                 new Product{Name ="Dixon Camera",Price= 280.75, Region ="Asia" },
                  new Product{Name ="Canon Lens",Price= 800.55, Region ="Europe" },
                   new Product{Name ="Beats Headphone",Price= 2000.15, Region ="Asia" },

                   new Product{Name ="Nikon Camera",Price= 298.35, Region ="Asia" },
                   new Product{Name ="Fire Lighter",Price= 100.00, Region ="Africa" },
                   new Product{Name ="Braai Stand",Price= 656.12, Region ="Europe" },
                  new Product{Name ="Bike",Price= 1900.85, Region ="Asia" },
            }).ToList();
        }

        public static IList<Company> CreateCompanies()
        {
            return (new []
            {
                new Company{Name ="Nikon" , Region = "Africa"},
                new Company{ Name = "Xiquel Group", Region = "Asia"},
                new Company{Name = "Fujitsu" , Region="Europe"},
                new Company{Name= "Google", Region = "Asia"}
            }).ToList();
        }
    }
}
