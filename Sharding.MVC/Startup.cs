﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sharding.MVC.Startup))]
namespace Sharding.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
